<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

// Solo enviar notificaciones a usuarios logueados.
Broadcast::channel('channel-notify', function ($user) {
    return $user !== null;
});

Broadcast::channel('channel-chat', function ($user) {
    if( $user !== null ) {
        return $user;
    }
});

Broadcast::channel('channel-chat.greet.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
