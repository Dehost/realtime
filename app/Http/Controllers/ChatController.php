<?php

namespace App\Http\Controllers;

use App\Events\GreetingSent;
use App\Events\MessageSent;
use App\User;
use Codeception\Command\Run;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showChat()
    {
        return view('chat.show');
    }

    public function messageReceived(Request $request)
    {
        $rules = [
            'message'=> 'required'
        ];

        $request->validate($rules);

        broadcast( new MessageSent($request->user(), $request->message) );
        
        return  response()->json('Mensaje enviado');
    }
    
    public function greetReceived(Request $request, User $user)
    {
        broadcast( new GreetingSent($user, "{$request->user()->name} te saluda") );
        broadcast( new GreetingSent($request->user(), "saludaste a {$user->name}") );
        
        return  response()->json('Saludos correctos');
    }
}
