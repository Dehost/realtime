<?php

namespace App\Console\Commands;

use App\Events\RemainingTimeChanged;
use App\Events\WinnerNumberGenerate;
use Illuminate\Console\Command;

class GameStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'inicio del juego';
    private $time = 15;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while (true) {
            // envia los segundos donde inicia.
            broadcast( new RemainingTimeChanged($this->time.'s') );
            
            $this->time--;
            sleep(1);
            
            if ( $this->time == 0 ) {
                
                $this->time = "Esperando el nuevo inicio dle juego";

                broadcast( new RemainingTimeChanged( $this->time ) );

                broadcast( new WinnerNumberGenerate( rand(1, 10) ) );
                sleep(5);

                $this->time = 15;
            }
        }
    }
}
